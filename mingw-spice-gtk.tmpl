%{?mingw_package_header}

Name: $PKG_NAME
Version: $PKG_VERSION
Release: $TIMESTAMP$PKG_RELEASE%{?dist}

Summary: A GTK+ widget for SPICE clients

Group: User Interface/Desktops
License: LGPLv2+
URL: https://www.spice-space.org/

Source0: $PKG_ARCHIVE

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

BuildRequires: mingw32-filesystem >= 95
BuildRequires: mingw64-filesystem >= 95
BuildRequires: mingw32-gcc
BuildRequires: mingw64-gcc
BuildRequires: mingw32-binutils
BuildRequires: mingw64-binutils
BuildRequires: glib2-devel

BuildRequires: mingw32-gtk3 >= 2.91.3
BuildRequires: mingw64-gtk3 >= 2.91.3
BuildRequires: mingw32-pixman
BuildRequires: mingw64-pixman
BuildRequires: mingw32-openssl
BuildRequires: mingw64-openssl
BuildRequires: mingw32-libjpeg-turbo
BuildRequires: mingw64-libjpeg-turbo
BuildRequires: mingw32-celt051
BuildRequires: mingw64-celt051
BuildRequires: mingw32-zlib
BuildRequires: mingw64-zlib
BuildRequires: mingw32-gstreamer1
BuildRequires: mingw64-gstreamer1
BuildRequires: mingw32-gstreamer1-plugins-base
BuildRequires: mingw64-gstreamer1-plugins-base
BuildRequires: mingw32-opus
BuildRequires: mingw64-opus
BuildRequires: mingw32-spice-protocol >= 0.12.11
BuildRequires: mingw64-spice-protocol >= 0.12.11
BuildRequires: mingw32-usbredir
BuildRequires: mingw64-usbredir

# Hack because of bz #613466
BuildRequires: intltool
BuildRequires: libtool

BuildRequires: autoconf
BuildRequires: automake

%description
Client libraries for SPICE desktop servers.


# Mingw32
%package -n mingw32-spice-gtk3
Summary: %{summary}
Requires: mingw32-spice-glib = %{version}-%{release}
Requires: mingw32-gtk3
Requires: pkgconfig
Obsoletes: mingw32-spice-gtk < 0.32
Obsoletes: mingw32-spice-gtk-static < 0.32-2

%description -n mingw32-spice-gtk3
Gtk+3 client libraries for SPICE desktop servers.

%package -n mingw32-spice-glib
Summary: GLib-based library to connect to SPICE servers
Requires: pkgconfig
Requires: mingw32-glib2
Requires: mingw32-spice-protocol

%description -n mingw32-spice-glib
A SPICE client library using GLib2.

# Mingw64
%package -n mingw64-spice-gtk3
Summary: %{summary}
Requires: mingw64-spice-glib = %{version}-%{release}
Requires: mingw64-gtk3
Requires: pkgconfig
Obsoletes: mingw64-spice-gtk < 0.32
Obsoletes: mingw64-spice-gtk-static < 0.32-2

%description -n mingw64-spice-gtk3
Gtk+3 client libraries for SPICE desktop servers.

%package -n mingw64-spice-glib
Summary: GLib-based library to connect to SPICE servers
Requires: pkgconfig
Requires: mingw64-glib2
Requires: mingw64-spice-protocol

%description -n mingw64-spice-glib
A SPICE client library using GLib2.

%{?mingw_debug_package}



%prep
%setup -q -n $PKG_ARCHIVE_NO_EXTENSION

%build
%mingw_configure                                \
    --without-sasl                              \
    --with-coroutine=winfiber                   \
    --disable-smartcard                         \
    --enable-usbredir                           \
    --disable-gtk-doc                           \
    --disable-werror


%install
rm -rf \$RPM_BUILD_ROOT

%mingw_make install "DESTDIR=\$RPM_BUILD_ROOT"

# Libtool files don't need to be bundled
find \$RPM_BUILD_ROOT -name "*.la" -delete
# man pages don't need to be bundled
find \$RPM_BUILD_ROOT -name "*.1" -delete

%mingw_find_lang spice-gtk

%clean
rm -rf \$RPM_BUILD_ROOT

# Mingw32
%files -n mingw32-spice-glib -f spice-gtk.lang
%defattr(-,root,root)
%doc AUTHORS
%doc ChangeLog
%doc COPYING
%doc README
%doc NEWS
%{mingw32_bindir}/libspice-client-glib-2.0-8.dll
%{mingw32_bindir}/libspice-controller-0.dll
%{mingw32_bindir}/spicy-screenshot.exe
%{mingw32_bindir}/spicy-stats.exe
%{mingw32_libdir}/libspice-client-glib-2.0.dll.a
%{mingw32_libdir}/libspice-controller.dll.a
%{mingw32_libdir}/pkgconfig/spice-client-glib-2.0.pc
%{mingw32_libdir}/pkgconfig/spice-controller.pc
%{mingw32_includedir}/spice-client-glib-2.0
%{mingw32_includedir}/spice-controller
%{mingw32_datadir}/vala/vapi

%files -n mingw32-spice-gtk3
%defattr(-,root,root)
%{mingw32_bindir}/libspice-client-gtk-3.0-5.dll
%{mingw32_bindir}/spicy.exe
%{mingw32_libdir}/libspice-client-gtk-3.0.dll.a
%{mingw32_libdir}/pkgconfig/spice-client-gtk-3.0.pc
%{mingw32_includedir}/spice-client-gtk-3.0

# Mingw64
%files -n mingw64-spice-glib -f spice-gtk.lang
%defattr(-,root,root)
%doc AUTHORS
%doc ChangeLog
%doc COPYING
%doc README
%doc NEWS
%{mingw64_bindir}/libspice-client-glib-2.0-8.dll
%{mingw64_bindir}/libspice-controller-0.dll
%{mingw64_bindir}/spicy-screenshot.exe
%{mingw64_bindir}/spicy-stats.exe
%{mingw64_libdir}/libspice-client-glib-2.0.dll.a
%{mingw64_libdir}/libspice-controller.dll.a
%{mingw64_libdir}/pkgconfig/spice-client-glib-2.0.pc
%{mingw64_libdir}/pkgconfig/spice-controller.pc
%{mingw64_includedir}/spice-client-glib-2.0
%{mingw64_includedir}/spice-controller
%{mingw64_datadir}/vala/vapi

%files -n mingw64-spice-gtk3
%defattr(-,root,root)
%{mingw64_bindir}/libspice-client-gtk-3.0-5.dll
%{mingw64_bindir}/spicy.exe
%{mingw64_libdir}/libspice-client-gtk-3.0.dll.a
%{mingw64_libdir}/pkgconfig/spice-client-gtk-3.0.pc
%{mingw64_includedir}/spice-client-gtk-3.0
