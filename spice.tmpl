Name: $PKG_NAME
Version: $PKG_VERSION
Release: $TIMESTAMP$PKG_RELEASE%{?dist}

Summary: Implements the SPICE protocol

Group: User Interface/Desktops
License: LGPLv2+
URL: https://www.spice-space.org/

Source0: $PKG_ARCHIVE

BuildRequires:  glib2-devel >= 2.28
BuildRequires:  spice-protocol >= 0.12.12
BuildRequires:  celt051-devel
BuildRequires:  opus-devel
BuildRequires:  pixman-devel alsa-lib-devel openssl-devel libjpeg-devel
BuildRequires:  libcacard-devel cyrus-sasl-devel
%if 0%{?fedora} >= 25
BuildRequires:  python3 python3-six python3-pyparsing
%else
BuildRequires:  pyparsing
BuildRequires:  python-six
%endif
BuildRequires:  gnupg2
BuildRequires:  lz4-devel
BuildRequires:  gstreamer1-devel gstreamer1-plugins-base-devel

%description
The Simple Protocol for Independent Computing Environments (SPICE) is a remote display system built for virtual environments which allows you to view a computing 'desktop' environment not only on the machine where it is running, but from anywhere on the Internet and from a wide variety of machine architectures.

%package server
Summary: Implements the server side of the SPICE protocol
Group: System Environment/Libraries
Obsoletes: spice-client < %{version}-%{release}

%description server
The Simple Protocol for Independent Computing Environments (SPICE) is
a remote display system built for virtual environments which allows
you to view a computing 'desktop' environment not only on the machine
where it is running, but from anywhere on the Internet and from a wide
variety of machine architectures.

%package server-devel
Summary: Header files, libraries and development documentation for spice-server
Group: Development/Libraries
Requires: %{name}-server%{?_isa} = %{version}-%{release}
Requires: pkgconfig

%description server-devel
This package contains the header files, static libraries and development
documentation for spice-server. If you like to develop programs
using spice-server, you will need to install spice-server-devel.

%prep
%setup -q -n $PKG_ARCHIVE_NO_EXTENSION

%build
%define configure_client --disable-client
%configure --enable-smartcard %{configure_client}
make %{?_smp_mflags} WARN_CFLAGS='' V=1

%install
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_libdir}/libspice-server.a
rm -f %{buildroot}%{_libdir}/libspice-server.la
mkdir -p %{buildroot}%{_libexecdir}


%post server -p /sbin/ldconfig
%postun server -p /sbin/ldconfig


%files server
%{!?_licensedir:%global license %%doc}
%license COPYING
%doc README NEWS
%{_libdir}/libspice-server.so.1*

%files server-devel
%{_includedir}/spice-server
%{_libdir}/libspice-server.so
%{_libdir}/pkgconfig/spice-server.pc
